$(function () {
    $('.graph1').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -60,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [
                ['Иванова К.К.', 10],
                ['Иванов Г.а.', 16.1],
                ['Боброва Е.Р.', 14.2],
                ['Петрова Г.А.', 14.0],
                ['Бобров Г.Г.', 12.5],
                ['Иванов', 12.1],
                ['Иванова', 11.8],
                ['Бобров', 11.7],
                ['Петрова', 11.1]                       
            ],
            colors: ["#000", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee", "#55BF3B", "#DF5353"],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.1f}', 
                y: 0,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });


    $('.graph2').highcharts({
       chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [
                ['Иванова К.К.', 10],
                ['Иванов Г.а.', 16.1],
                ['Боброва Е.Р.', 14.2],
                ['Петрова Г.А.', 14.0],
                ['Бобров Г.Г.', 12.5],
                ['Иванов', 12.1],
                ['Иванова', 11.8],
                ['Бобров', 11.7],
                ['Петрова', 11.1]
            ],
            colors: ["#000", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee", "#55BF3B", "#DF5353"],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.1f}', 
                y: 0,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('.graph3').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [
                ['Иванова К.К.', 10],
                ['Иванов Г.а.', 16.1],
                ['Боброва Е.Р.', 14.2],
                ['Петрова Г.А.', 14.0],
                ['Бобров Г.Г.', 12.5],
                ['Иванов', 12.1],
                ['Иванова', 11.8],
                ['Бобров', 11.7],
                ['Петрова', 11.1]                       
            ],
            colors: ["#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb"],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.1f}', 
                y: 0,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('.graph4').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -60,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: ''
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6], 
        }, {
            data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]           
        }]
    });

    $('.graph5').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Browser market shares January, 2015 to May, 2015'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [
                ['Иванова К.К.', 10],
                ['Иванов Г.а.', 16.1],
                ['Боброва Е.Р.', 14.2],
                ['Петрова Г.А.', 14.0],
                ['Бобров Г.Г.', 12.5],
                ['Иванов', 12.1],
                ['Иванова', 11.8],
                ['Бобров', 11.7],
                ['Петрова', 11.1]                       
            ],
            colors: ["#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb", "#ff59cb"],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.1f}', 
                y: 0,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

    $('.graph6').highcharts({
       chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' '
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            name: '',
            colorByPoint: true,
            data: [
                ['Иванова К.К.', 10],
                ['Иванов Г.а.', 16.1],
                ['Боброва Е.Р.', 14.2],
                ['Петрова Г.А.', 14.0],
                ['Бобров Г.Г.', 12.5],
                ['Иванов', 12.1],
                ['Иванова', 11.8],
                ['Бобров', 11.7],
                ['Петрова', 11.1]
            ],
            colors: ["#000", "#8085e9", "#8d4654", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee", "#55BF3B", "#DF5353"],
            dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                align: 'center',
                format: '{point.y:.1f}', 
                y: 0,
                style: {
                    fontSize: '12px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
    
});

